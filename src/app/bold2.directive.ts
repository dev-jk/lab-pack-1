import {Directive, ElementRef, Renderer2, HostListener, HostBinding} from '@angular/core';
 
@Directive({
    selector: '[bold2]',
    host: {
        '(click)': 'onMouseClick()'       
    }
})
export class Bold2Directive{

    private fontWeight = "normal";
     
    constructor(private elementRef: ElementRef, private renderer: Renderer2){
         
        this.renderer.setStyle(this.elementRef.nativeElement, "font-weight", "bold");
    }

    onMouseClick(){
        this.setFontColor("Red");
    }

    @HostBinding("style.fontWeight") get getFontWeight(){
         
        return this.fontWeight;
    }
     
    @HostBinding("style.cursor") get getCursor(){
        return "pointer";
    }

    @HostListener("mouseenter") onMouseEnter() {
        this.setFontWeight("bold");
    }
 
    @HostListener("mouseleave") onMouseLeave() {
        this.setFontWeight("normal");
    }

    private setFontWeight(val: string) {
        this.renderer.setStyle(this.elementRef.nativeElement, "font-weight", val);
    }

    private setFontColor(val: string) {
        this.renderer.setStyle(this.elementRef.nativeElement, "color", val);
    }


}