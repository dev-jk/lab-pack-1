import { Component} from '@angular/core';
import {DataService} from './data/data.service';
//import {LogService} from './log.service';

        
@Component({
    selector: 'my-app2',
    template: `<div [ngClass]="currentClasses">
                    <h1>Hello Angular</h1>
                    <p>
                        Angular представляет модульную архитектуру приложения
                    </p>
                </div>
                <div [ngStyle]="{'font-size':'13px', 'font-family':'Verdana'}">
                    <h1>Hello Angular 11</h1>
                    <p [ngStyle]="{'font-size':'14px', 'font-family':'Segoe Print'}">
                        Angular 11 представляет модульную архитектуру приложения
                    </p>
                </div>
                <div bold="blue" selectedSize="28px" [defaultSize]="'8px'" [ngClass]="{invisible: visibility}">Visibility and bold</div>
                <div bold2 [class.invisible]="visibility">Visibility2 and bold2</div>
                <div [style.display]="visibility? 'none' : 'block'">Visibility3</div>
                <button (click)="toggle()">Toggle</button>
                <p *ngIf="condition">
                  Привет мир
                </p>
                <p *ngIf="!condition">
                  Пока мир
                </p>
                <button (click)="toggle2()">Toggle2</button>
                <p *ngIf="condition;else unset">
                  Condition test
                </p>
                <ng-template #unset>  
                  <p>Test OK</p>  
                </ng-template>
                <div *ngIf="condition; then thenBlock else elseBlock"></div>   
                <ng-template #thenBlock>Then template</ng-template>  
                <ng-template #elseBlock>Else template</ng-template>  
                <ul>
                  <li *ngFor="let item of names">{{item}}</li>
                </ul>  
                <div>
                   <p *ngFor="let item of names; let i = index">{{i+1}}.{{item}}</p>
                </div>
                <div [ngSwitch]="count">
                  <ng-template ngSwitchCase="1">{{count * 10}}</ng-template>
                  <ng-template ngSwitchCase="2">{{count * 100}}</ng-template>
                  <ng-template ngSwitchDefault>{{count * 1000}}</ng-template>
                </div>
                <p *while="condition">
                  Первый параграф
                </p>
                <p *while="!condition">
                  Второй параграф
                </p>
                <div class="panel">
                    <div><input [(ngModel)]="name" placeholder = "Модель" />
                        <button (click)="addItem(name)">Добавить</button>
                    </div>
                    <table>
                        <tr *ngFor="let item of items">
                            <td>{{item}}</td>
                        </tr>
                    </table>
                </div>
                <test-comp></test-comp>
                <test-comp></test-comp>
                <form-comp></form-comp>
                <form2-comp></form2-comp>
                <val-comp></val-comp>
                <val-form-comp></val-form-comp>
                <val-form2-comp></val-form2-comp>
                <form-builder-comp></form-builder-comp>
                `,
    styles: [
        `.verdanaFont{font-size:13px; font-family:Verdana;}
        .navyColor{color:navy;}`,
        `.invisible{display:none;}`
    ],
    //providers: [DataService, LogService]


})


export class AppComponent2 { 

    names = ["Tom", "Bob", "Sam", "Bill"];
    items: string[] = [];
    
    name: string;

    count: number = 5;
    isVerdana = true;
    isNavy = true;
    visibility: boolean = true;
    condition: boolean = true;

    constructor(private dataService: DataService){}

    ngOnInit(){
        this.items = this.dataService.getData();
    }

    toggle(){
        this.visibility=!this.visibility;
    }

    toggle2(){
        this.condition=!this.condition;
    }

    addItem(name: string){
          
        this.dataService.addData(name);
    }
 
    currentClasses={
        verdanaFont: this.isVerdana,
        navyColor: this.isNavy
    }
}