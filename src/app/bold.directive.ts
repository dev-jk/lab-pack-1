import {Directive, ElementRef, Input, HostBinding, HostListener} from '@angular/core';
 
@Directive({
    selector: '[bold]'
})
export class BoldDirective{

    @Input() selectedSize = "18px";
    @Input() defaultSize = "16px";
    @Input("bold") selectedColor = "Green";

    private color : string;
    private fontSize : string;
    
    ngOnInit(){
        this.fontSize = this.defaultSize;
    }

    @HostBinding("style.color") get getColor(){
         
        return this.color;
    }

    @HostBinding("style.fontSize") get getFontSize(){
         
        return this.fontSize;
    }

    @HostListener("mouseenter") onMouseEnter() {     
        this.color = this.selectedColor;
        this.fontSize = this.selectedSize;
    }
 
    @HostListener("mouseleave") onMouseLeave() {      
        this.fontSize = this.defaultSize;
        this.color = "black"
    }
     
    constructor(private elementRef: ElementRef){
         
        this.elementRef.nativeElement.style.fontWeight = "bold";
    }
}