import { NgModule, Testability }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { AppComponent2 }   from './app.component2';
import { DataModule }   from './data/data.module';
import { ChildComponent } from './child.component';
import { BoldDirective } from './bold.directive';
import { Bold2Directive } from './bold2.directive';
import { WhileDirective } from './while.directive';
import { FormsModule } from '@angular/forms';
import { TestComponent } from './test.component';
import {DataService} from './data/data.service';
import {LogService} from './log.service';
import {FormComponent} from './form.component';
import {Form2Component} from './form2.component';
import {ValidationComponent} from './validation.component';
import {ValidationFormComponent} from './validation-form.component';
import {ValidationForm2Component} from './validation-form2.component';
import { ReactiveFormsModule }   from '@angular/forms';
import { FormBuilderComponent } from './from-builder.component';

 
@NgModule({
    imports:      [ BrowserModule, DataModule, ReactiveFormsModule, FormsModule],
    declarations: [ AppComponent, AppComponent2, ChildComponent, BoldDirective, Bold2Directive, WhileDirective, TestComponent, FormComponent, Form2Component, ValidationComponent, ValidationFormComponent, ValidationForm2Component, FormBuilderComponent],
    providers: [DataService, LogService],
    bootstrap:    [ AppComponent2 ]
})
export class AppModule { }